<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.sql.ConnectionEvent"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.io.BufferedReader"%>
<%@page import ="java.io.IOException" %>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import=" java.net.URLEncoder"%>
<!DOCTYPE html>

<html lang="">
<head>
<title>Dr.Visit</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> +91 123456790</li>
        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
      </ul>
    </div>
  </div>
</div>
  <div class="fl_right">
      <ul>
			<li><i class="drop"></i> Profile
        <ul>
        <li><a href="patient_profile.jsp">User</a></li>
          <li><a  href="dr_profile.jsp">Doctor</a></li>
          </ul>
          <%
         String uid = (String)session.getAttribute("username");
          if(uid!=null)
          {
           %>
        <li><a href="logout.jsp">Logout</a></li>
        <%} %>
         </li>
	        <li><a href="all_login.html">Login</a></li>
         <li><a href="admin.html">Admin</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <div id="logo" class="fl_left">
      <h1><a href="index.html">Dr.Visit</a></h1>
      <p>Onsite Doctor Visit System</p>
    </div>
    <div id="quickinfo" class="fl_right">
      <ul class="nospace inline">
        <li><strong>Contact No:</strong><br>
          +91 123456790</li>
        
      </ul>
    </div>
  </header>
  <nav id="mainav" class="hoc clear"> 
     <ul class="clear">
      <li class="active"><a href="index.html">Home</a></li>
      <li><a class="drop" href="Doctor.jsp">Doctors</a>
       <ul>
        <table>
          <tr><td>
          <li><a href="search.jsp?Id=dentist">Dentists</a></li>
          <li><a  href="search.jsp?Id=gynecologist">Gynecologist</a></li>
          <li><a href="search.jsp?Id=dermatologist">Dermatologist</a></li>
          </td>
          <td>
          <li><a  href="search.jsp?Id=neurologist">Neurologist</a></li>
          <li><a href="search.jsp?Id=physiatrist">Physiatrist</a></li>
          <li><a  href="search.jsp?Id=plastic surgeon">Plastic surgeon</a></li>
          </td>
          <td>
          <li><a href="search.jsp?Id=Psychiatrist">Psychiatrist</a></li>
          <li><a  href="search.jsp?Id=radiologist">Radiologist</a></li>
          </td>
          </tr>
        </table></ul>
      </li>
      <li><a href="#">Book an appointment</a></li>
     
    </ul>
  </nav>
</div>

<div class="wrapper bgded overlay" style="background-image:url('images/mediback2.jpg');">


<br>
<br>
<br>
<br>
<br>
<br>
<%
if(uid==null)
{
	%>
	<script type="text/javascript">
	window.location.replace("patient-login.html");
	alert("You need to login first");
	</script>
	<%
}
else
{
String dr=request.getParameter("Id");
String dr_no=request.getParameter("Id1");
String mobiles=uid;
String dr_mobile=dr_no;
System.out.print(mobiles);
System.out.print(dr_mobile);

System.out.println(mobiles);
String authkey = "280060AWZFUsbnaCH5cfa89aa";
String senderId = "DRVIST";
//Your message to send, Add URL encoding here.
String message = "You have booked an appointment with Dr."+dr+".The doctor will reach you in estimated half an hour from now. Thank you.";
String route="4";
URLConnection myURLConnection=null;
URL myURL=null;
BufferedReader reader=null;
//encoding message
String encoded_message=URLEncoder.encode(message);
String mainUrl="https://control.msg91.com/api/sendhttp.php?";//Your sms gateway provider API
//Prepare parameter string
StringBuilder sbPostData= new StringBuilder(mainUrl);
sbPostData.append("authkey="+authkey);
sbPostData.append("&mobiles="+mobiles);
sbPostData.append("&message="+encoded_message);
sbPostData.append("&route="+route);
sbPostData.append("&sender="+senderId);
mainUrl = sbPostData.toString();
//final string

URLConnection dr_myURLConnection=null;
URL dr_myURL=null;
BufferedReader dr_reader=null;
Class.forName("com.mysql.jdbc.Driver");
Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/users?useSSL=false","root","root@1234");
Statement s=conn.createStatement();
ResultSet rs=s.executeQuery("select uname from user where mob_no='"+uid+"'");
String patient=null;
while(rs.next())
	  patient=rs.getString(1);
String message1="You have an appointment with "+patient+" .To contact call "+uid+"";
String dr_encoded_message=URLEncoder.encode(message1);
String dr_mainUrl="https://control.msg91.com/api/sendhttp.php?";//Your sms gateway provider API
//Prepare parameter string
StringBuilder dr_sbPostData= new StringBuilder(dr_mainUrl);
sbPostData.append("authkey="+authkey);
sbPostData.append("&mobiles="+dr_mobile);
sbPostData.append("&message="+dr_encoded_message);
sbPostData.append("&route="+route);
sbPostData.append("&sender="+senderId);
dr_mainUrl = sbPostData.toString();
try
{
//prepare connection
myURL = new URL(mainUrl);
myURLConnection = myURL.openConnection();
myURLConnection.connect();
System.out.println(myURL);
reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

dr_myURL = new URL(dr_mainUrl);
myURLConnection = dr_myURL.openConnection();
myURLConnection.connect();
System.out.println(dr_myURL);
dr_reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
String success="Your message sent sucessfully";

if(!conn.isClosed())
{
	
	Statement s1=conn.createStatement();
	int rs1=s1.executeUpdate("insert into history(mob_no,dr_mob_no,date_time,status) values ('"+uid+"','"+dr_no+"',now(),'Booked')");
}
System.out.println(success);
//finally close connection
reader.close();

%>
<h2>You have successfully booked an appointment with Dr.<%=dr%></h2>
<% 
}
catch (IOException e)
{
e.printStackTrace();
}
}
%>
</br>
</br>
</br></br>
</br>
</br>
</br>
</br>
</br></br>
</br>
</br>
</br>
</br>
</br></br>
</br>
</div>
<div class="wrapper row4 bgded overlay" style="background-image:url('images/doc3.jpg');">
  <footer id="footer" class="hoc clear"> 
    
    <div class="one_third">
      <h6 class="heading">Dr.Visit</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          Mit Pune
          </address>
        </li>
        <li><i class="fa fa-phone"></i> +91 1234567890</li>
        
        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul>
    </div>
   
  </footer>
</div>

</body>
</html>