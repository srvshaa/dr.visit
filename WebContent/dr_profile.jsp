<!DOCTYPE html>
    <%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>

<%@page import="java.sql.Connection"%>

<%@page import="javax.sql.ConnectionEvent"%>
<%@page import="java.sql.DriverManager"%>
<html lang="">
<head>
<link href="https://cdn.jotfor.ms/static/formCss.css?3.3.11961" rel="stylesheet" type="text/css" />
<link type="text/css" media="print" rel="stylesheet" href="https://cdn.jotfor.ms/css/printForm.css?3.3.11961" />
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/nova.css?3.3.11961" />
<link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/themes/CSS/566a91c2977cdfcd478b4567.css?themeRevisionID=58c6459d9a11c7136a8b4567"/>
<style type="text/css">

body
{
	background-color:#2196F3;
	background-image: url("images/mediback2.jpg"); 
}

</style>


<style type="text/css">
    .form-label-left{
        width:150px;
		margin-left:150px;
    }
    .form-line{
        padding-top:10px;
        padding-bottom:10px;
    }
    .form-label-right{
        width:150px;
    }
    .form-all{
        width:600px;
        color:#3E4E1A !important;
        font-family:"Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Verdana, sans-serif;
        font-size:14px;
		align:center;
    }
   

</style>

<style type="text/css" id="form-designer-style">

    .form-all {
	 width: 100%;
      font-family: Lucida Grande, sans-serif;
	  font-size: 14px;
	  color:#363636 ;
	  float: center;
	  padding-bottom: 50px;
    }
  
    .form-all .form-pagebreak-back-container,
    .form-all .form-pagebreak-next-container {
      font-family: Lucida Grande, sans-serif;
    }
    .form-header-group {
      font-family: Lucida Grande, sans-serif;
    }
    .form-label {
      font-family: Lucida Grande, sans-serif;
    }
  
    .form-label.form-label-auto {
      
    display: inline-block;
    float: left;
    text-align: left;
  
    }
  
    .form-line {
      margin-top: 10px;
      margin-bottom: 10px;
    }
  
 
  
    .form-label-left,
    .form-label-right,
    .form-label-left.form-label-auto,
    .form-label-right.form-label-auto {
      width: 150px;
    }
 
  
  
    .form-all, .form-all {
      background-color: white;
      border: 1px solid transparent;
    }
  
    
    .form-header-group .form-header {
      color: #3E4E1A;
	  margin-left:150px;
    }
    .form-header-group .form-subHeader {
      color: #3E4E1A;
    }
    .form-label-top,
    .form-label-left,
    .form-label-right,
    .form-html,
    {
      color: #3E4E1A;
    }
    
  .ie-8 .form-all:before { display: none; }
  .ie-8 {
    margin-top: auto;
    margin-top: initial;
  }
  

</style>

<title>Dr.Visit</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
</head>
<body id="top">

<div class="wrapper row0">
  <div id="topbar" class="hoc clear"> 
    <div class="fl_left">
      <ul>
        <li><i class="fa fa-phone"></i> +91 123456790</li>
        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul>
    </div>
    <div class="fl_right">
      <ul>
        <li><a href="#"><i class="fa fa-lg fa-home"></i></a></li>
        <%
         String uid = (String)session.getAttribute("username");
          if(uid!=null)
          {
           %>
        <li><a href="logout.jsp">Logout</a></li>
        <%} %>
        
      </ul>
    </div>
  </div>
</div>

<div class="wrapper row1">
  <header id="header" class="hoc clear"> 
    <div id="logo" class="fl_left">
      <h1><a href="index.html">Dr.Visit</a></h1>
      <p>Onsite Doctor Visit System</p>
    </div>
    <div id="quickinfo" class="fl_right">
     <ul class="nospace inline">
        <li><strong>Contact No:</strong><br>
          +91 123456790</li>
        
      </ul>
    </div>
  </header>
  

<nav id="mainav" class="hoc clear"> 
     <ul class="clear">
      <li class="active"><a href="index.html">Home</a></li>
      <li><a href="Doctor.jsp">Doctors</a>
      <ul>
        <table>
          <tr><td>
          <li><a href="search.jsp?Id=dentist">Dentists</a></li>
          <li><a  href="search.jsp?Id=gynecologist">Gynecologist</a></li>
          <li><a href="search.jsp?Id=dermatologist">Dermatologist</a></li>
          </td>
          <td>
          <li><a  href="search.jsp?Id=neurologist">Neurologist</a></li>
          <li><a href="search.jsp?Id=physiatrist">Physiatrist</a></li>
          <li><a  href="search.jsp?Id=plastic surgeon">Plastic surgeon</a></li>
          </td>
          <td>
          <li><a href="search.jsp?Id=Psychiatrist">Psychiatrist</a></li>
          <li><a  href="search.jsp?Id=radiologist">Radiologist</a></li>
          </td>
          </tr>
        </table></ul>
      <li><a href="Doctor.jsp">Book an appointment</a></li>     
     
    </ul>
  </nav>

</div>
<%
if(uid==null)
{
%>
<script type="text/javascript">
	window.location.replace("doctor-login.html");
	alert("You need to login first");
	</script>
	<%
}
else
Class.forName("com.mysql.jdbc.Driver");
Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/users?useSSL=false","root","root@1234");
if(!conn.isClosed())
{
	
	Statement s=conn.createStatement();
	ResultSet rs=s.executeQuery("select * from dr_details,dr_timings,location_details,edu_info  where dr_details.mob_no='"+uid+"' and dr_details.mob_no=dr_timings.mob_no and dr_timings.mob_no=location_details.mob_no and location_details.mob_no=edu_info.mob_no;");	
	while(rs.next())
	{
	%>
<form >
  <input type="hidden" name="formID" />
  <div role="main" class="form-all">
    <ul class="form-section page-section">
      <li id="cid_10" class="form-input-wide" data-type="control_head">
        <div class="form-header-group ">
          <div class="header-text httal htvam">
            <h2 id="header_10" class="form-header" data-component="header">
              Doctor Profile
            </h2>
          </div>
        </div>
      </li>   
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
         Name:
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(2)%> 
        </div>
      </li>
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto">
          Email :      
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(3)%> 
        </div>
        <div id="email" class="form-input jf-required">
        </div>
      </li>
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
          Gender :
           </label>
          <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(4)%> 
        </div>
        </label>
        <div id="gender" class="form-input jf-required">
        </div>
      </li>
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
          Mobile Number :
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getLong(1)%> 
        </div>
        <div id="pno" class="form-input jf-required">
        </div>
      </li>
      <li class="form-line jf-required">
        <label class="form-label form-label-left form-label-auto">
          Address :
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(13)%> 
        </div>
		<div id="addr" class="form-input jf-required">		
        </div>  
      </li>
      <li class="form-line jf-required">
        <label class="form-label form-label-left form-label-auto">
          Degree :
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(15)%> 
        </div>
		<div id="addr" class="form-input jf-required">		
        </div>  
      </li>  
      <li class="form-line jf-required">
        <label class="form-label form-label-left form-label-auto">
          Speciality :
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(16)%> 
        </div>
		<div id="addr" class="form-input jf-required">		
        </div>  
      </li>
      <li class="form-line jf-required">
        <label class="form-label form-label-left form-label-auto">
         Work Experience:
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs.getString(17)%> 
        </div>
		<div id="addr" class="form-input jf-required">		
        </div>  
      </li>
      <li id="cid_10" class="form-input-wide" data-type="control_head">
        <div class="form-header-group ">
          <div class="header-text httal htvam">
            <h2 id="header_10" class="form-header" data-component="header">
              History
            </h2>
         </div>
        </div>
      </li>
        <%
}

Statement s1=conn.createStatement();
	ResultSet rs1=s1.executeQuery("select * from history where dr_mob_no='"+uid+"'; ");
	
	while(rs1.next())
	{
	%>
       <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
          Patient:
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%
	      Statement s2=conn.createStatement();
	  	ResultSet rs2=s2.executeQuery("select uname from user where mob_no='"+rs1.getLong(1)+"' ");
	    while(rs2.next())  
	  	out.print(rs2.getString(1));
	      %> 
        </div>
      </li>
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
          Time:
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs1.getString(3)%> 
        </div>
      </li>
      <li class="form-line jf-required" >
        <label class="form-label form-label-left form-label-auto" >
          Status:
        </label>
        <div id="usernm" class="form-input jf-required">
	      <%=rs1.getString(4)%> 
        </div>
      </li>
  
    </ul>
  </div> 
  
  <%
}
}%>
</form>
<br>
</br>
</br>
</br>
</br>
</br>
</br></br>
</br>
</br>
</br>

<div class="wrapper row4 bgded overlay" style="background-image:url('images/doc3.jpg');">
  <footer id="footer" class="hoc clear"> 
    <div class="one_third">
      <h6 class="heading">Dr.Visit</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>
          Mit Pune
          </address>
        </li>
        <li><i class="fa fa-phone"></i> +91 1234567890</li>
        
        <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
      </ul>
    </div>
   
  </footer>
</div>


</body>
</html>